//
//  Configs.swift
//  Purchases
//
//  Created by Денис on 05/02/2019.
//  Copyright © 2019 Денис. All rights reserved.
//

import Foundation
import UIKit

// HUD VIEW
let hudView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
let label = UILabel()
let indicatorView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
extension UIViewController {
    func showHUD(_ message:String) {
        hudView.center = CGPoint(x: view.frame.size.width/2, y: view.frame.size.height/2)
        hudView.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        hudView.alpha = 0.7
        hudView.layer.cornerRadius = 8
        
        indicatorView.center = CGPoint(x: hudView.frame.size.width/2, y: hudView.frame.size.height/2)
        indicatorView.style = UIActivityIndicatorView.Style.whiteLarge
        hudView.addSubview(indicatorView)
        indicatorView.startAnimating()
        view.addSubview(hudView)
        
        label.frame = CGRect(x: 0, y: 70, width: 100, height: 30)
        label.font = UIFont(name: "HelveticaNeue-Light", size: 13)
        label.text = message
        label.textAlignment = .center
        label.textColor = UIColor.white
        hudView.addSubview(label)
    }
    
    func hideHUD() {
        hudView.removeFromSuperview()
        label.removeFromSuperview()
    }
    
    func simpleAlert(_ mess:String) {
        UIAlertView(title: "Purchases", message: mess, delegate: nil, cancelButtonTitle: "OK").show()
    }
}
